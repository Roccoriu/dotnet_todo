using System.Text.Json;

namespace TodoApp.Endpoints;

public static class RootEndpoints {
    public static RouteGroupBuilder MapEndpoints(this RouteGroupBuilder group) {
        group.MapGroup("/todos").MapTodoEndpoints();


        group.MapFallback(async context => {
            context.Response.StatusCode = StatusCodes.Status404NotFound;
            context.Response.ContentType = "application/json";
            await context.Response.WriteAsync(
                JsonSerializer.Serialize(new { Message = "Page not found" }
                )
            );
        });

        return group;
    }
}