using Microsoft.EntityFrameworkCore;
using TodoApp.Models;

namespace TodoApp.Data;

public class AppDbContext : DbContext {
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }


    public DbSet<User> Users { get; set; }
    public DbSet<TodoItem> TodoItems { get; set; }
    public DbSet<Category> Categories { get; set; }


    protected override void OnModelCreating(ModelBuilder modelBuilder) {
        base.OnModelCreating(modelBuilder);


        // Define a default user
        var defaultUser = new User {
            Id = 1,
            Username = "DefaultUser",
            Email = "default@example.com",
            PasswordHash = "hashedPassword"
        };
        modelBuilder.Entity<User>().HasData(defaultUser);

        // Define a default category
        var defaultCategory = new Category {
            Id = 1,
            Name = "DefaultCategory"
        };
        modelBuilder.Entity<Category>().HasData(defaultCategory);

        // Define some default todos
        var defaultTodos = new List<TodoItem> {
            new TodoItem {
                Id = 1,
                Title = "First Task",
                Description = "This is the first task",
                IsComplete = false,
                UserId = 1,
                CategoryId = 1
            },
            new TodoItem {
                Id = 2,
                Title = "Second Task",
                Description = "This is the second task",
                IsComplete = false,
                UserId = 1,
                CategoryId = 1
            },
            new TodoItem {
                Id = 3,
                Title = "Third Task",
                Description = "This is the third task",
                IsComplete = false,
                UserId = 1,
                CategoryId = 1
            },
        };
        modelBuilder.Entity<TodoItem>().HasData(defaultTodos);
    }
}