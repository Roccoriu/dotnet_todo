using FluentValidation;
using Microsoft.AspNetCore.Http.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using TodoApp.Data;
using TodoApp.Endpoints;
using TodoApp.Middleware;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("Default");
var corsOrigins = builder.Configuration.GetSection("CorsOrigin").Get<string[]>();

builder.Services.AddDbContext<AppDbContext>(
    options => options.UseSqlServer(connectionString)
);

builder.Services.AddCors(options => {
    options.AddPolicy(
        "CorsPolicies",
        policy => policy.
            WithOrigins(corsOrigins ?? new[] { "*" }).
            AllowAnyMethod().
            AllowAnyHeader());
});

builder.Services.Configure<JsonOptions>(
    options => { options.SerializerOptions.AllowTrailingCommas = true; }
);

builder.Services.AddValidatorsFromAssemblyContaining<Program>();

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("CorsPolicies");
app.UseHttpsRedirection();
app.UseAuthorization();
app.UseMiddleware<JsonErrorMiddleware>();


app.MapGroup("/api/v1").MapEndpoints();

app.Run();