using FluentValidation.Results;

namespace TodoApp.Common;

public static class ValidationResultExtensions {
    public static IDictionary<string, string[]> ToErrorDictionary(this ValidationResult validationResult) {
        return validationResult.Errors.GroupBy(x => x.PropertyName).
            ToDictionary(
                x => x.Key,
                x => x.Select(e => e.ErrorMessage).ToArray()
            );
    }
}