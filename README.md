# Testcases

- [x] JSON with trailing comma
- [x] Improperly formatted json
- [x] Missing fields for successful creation of the model
- [x] Empty Post/Put/Patch requests
