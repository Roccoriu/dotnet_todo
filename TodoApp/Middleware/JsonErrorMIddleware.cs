using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

namespace TodoApp.Middleware;

public class JsonErrorMiddleware {
    private readonly RequestDelegate _next;

    public JsonErrorMiddleware(RequestDelegate next) => _next = next;

    public async Task InvokeAsync(HttpContext context) {
        try {
            await _next(context);
        }
        catch (BadHttpRequestException ex) when (ex.InnerException is JsonException) {
            context.Response.Clear();
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            context.Response.ContentType = "application/json";

            await context.Response.WriteAsJsonAsync(
                new ValidationProblemDetails(new Dictionary<string, string[]> {
                        { "Message", new[] { "Improperly formatted JSON" } }
                    }
                )
            );
        }
        catch (BadHttpRequestException ex) when (ex.Message.Contains("Required parameter")) {
            context.Response.Clear();
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            await context.Response.WriteAsJsonAsync(
                new { error = $"{context.Request.Method} request body must not be empty!" }
            );
        }
    }
}