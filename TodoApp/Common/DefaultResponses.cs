namespace TodoApp.Common;

public record DefaultResponse(string Message);