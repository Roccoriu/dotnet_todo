using Microsoft.EntityFrameworkCore;
using TodoApp.Data;

namespace TestTodo.TestUtils;

public sealed class UsingDbContext : IDisposable {
    private bool _disposed;
    public AppDbContext Context { get; }

    public UsingDbContext() {
        var options = new DbContextOptionsBuilder<AppDbContext>().
            UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()).
            Options;

        Context = new AppDbContext(options);
    }

    ~UsingDbContext() => Dispose(false);

    private void Dispose(bool disposing) {
        if (_disposed) return;
        if (disposing) Context.Dispose();

        _disposed = true;
    }

    public void Dispose() {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}