using System.Diagnostics.CodeAnalysis;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TodoApp.Common;
using TodoApp.Data;
using TodoApp.Models;

namespace TodoApp.Services;

public static class TodoService {
    [ProducesResponseType(typeof(List<GetTodoItemDto>), StatusCodes.Status200OK)]
    [SuppressMessage("ReSharper.DPA", "DPA0006: Large number of DB commands", MessageId = "count: 267")]
    public static async Task<IResult> GetTodoItemList(AppDbContext db) {
        return TypedResults.Ok(await db.TodoItems.
            Select(e => new GetTodoItemDto(
                    e.Id,
                    e.Title,
                    e.Description,
                    e.IsComplete,
                    e.Category.Name,
                    e.DueDate
                )
            ).
            ToListAsync()
        );
    }

    [ProducesResponseType(typeof(GetTodoItemDto), StatusCodes.Status200OK)]
    public static async Task<IResult> GetTodoItem(int id, AppDbContext db) {
        var todoItem = await db.TodoItems.
            Where(e => e.Id == id).
            Select(e => new GetTodoItemDto(
                    e.Id,
                    e.Title,
                    e.Description,
                    e.IsComplete,
                    e.Category.Name,
                    e.DueDate
                )
            ).
            FirstOrDefaultAsync();

        if (todoItem != null) return TypedResults.Ok(todoItem);

        return TypedResults.NotFound(
            new DefaultResponse(Message: $"Todo with id {id} not found")
        );
    }

    [ProducesResponseType(typeof(TodoItem), StatusCodes.Status201Created)]
    public static async Task<IResult> CreateTodoItem(
        [FromBody] CreateTodoItemDto todo,
        IValidator<CreateTodoItemDto> validator,
        AppDbContext db
    ) {
        var validated = await validator.ValidateAsync(todo);
        if (!validated.IsValid) return TypedResults.ValidationProblem(validated.ToErrorDictionary());

        var newTodo = new TodoItem() {
            Title = todo.Title,
            Description = todo.Description,
            IsComplete = todo.IsComplete,
            DueDate = todo.DueDate,
            UserId = 1
        };

        var newTodoItem = await db.TodoItems.AddAsync(newTodo);
        await db.SaveChangesAsync();

        return TypedResults.Ok(newTodoItem.Entity);
    }


    [ProducesResponseType(typeof(TodoItem), StatusCodes.Status200OK)]
    public static async Task<IResult> UpdateTodoItem(
        int id,
        [FromBody] CreateTodoItemDto todo,
        IValidator<CreateTodoItemDto> validator,
        AppDbContext db
    ) {
        var validated = await validator.ValidateAsync(todo);
        if (!validated.IsValid) return TypedResults.ValidationProblem(validated.ToErrorDictionary());


        var existing = await db.TodoItems.FindAsync(id);
        if (existing is null) {
            return TypedResults.NotFound(new { Message = $"Todo with id {id} not found" });
        }

        existing.UserId = 1;
        existing.Title = todo.Title;
        existing.Description = todo.Description;
        existing.DueDate = todo.DueDate;
        existing.IsComplete = todo.IsComplete;

        await db.SaveChangesAsync();

        return TypedResults.Ok(existing);
    }

    [ProducesResponseType(typeof(TodoItem), StatusCodes.Status200OK)]
    public static async Task<IResult> DeleteTodoItem(int id, AppDbContext db) {
        if (await db.TodoItems.FindAsync(id) is not { } todo) return TypedResults.NotFound();

        db.TodoItems.Remove(todo);
        await db.SaveChangesAsync();

        return TypedResults.Ok(todo);
    }
}