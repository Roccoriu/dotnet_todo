using TodoApp.Services;

namespace TodoApp.Endpoints;

public static class TodoEndpoints {
    public static RouteGroupBuilder MapTodoEndpoints(this RouteGroupBuilder group) {
        group.MapGet("/", TodoService.GetTodoItemList);
        group.MapPost("/", TodoService.CreateTodoItem);
        group.MapGet("/{id:int}", TodoService.GetTodoItem);
        group.MapPatch("/{id:int}", TodoService.UpdateTodoItem);
        group.MapDelete("/{id:int}", TodoService.DeleteTodoItem);


        return group;
    }
}