using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using TestTodo.TestUtils;
using TodoApp.Common;
using TodoApp.Models;
using TodoApp.Services;

namespace TestTodo.TodoItemTests;

public class GetTodoItemTests {
    private readonly TodoItem _data = new() {
        Id = 1,
        Title = "First Task",
        Description = "This is the first task",
        IsComplete = false,
        UserId = 1,
        CategoryId = 1
    };

    [Fact]
    public async Task GetTodoItem_ReturnsOk_WhenTodoItemExists() {
        await using var context = new UsingDbContext().Context;

        context.TodoItems.Add(_data);
        await context.SaveChangesAsync();

        var res = await TodoService.GetTodoItem(1, context);
        Assert.IsType<Ok<TodoItem>>(res);

        var okResult = (Ok<TodoItem>)res;

        Assert.NotNull(okResult.Value);
        Assert.Equal(1, okResult.Value.Id);
    }

    [Fact]
    public async Task GetTodoItem_ReturnsNotFound_WhenTodoItemDoesNotExist() {
        await using var context = new UsingDbContext().Context;

        var res = await TodoService.GetTodoItem(2, context);
        Assert.IsType<NotFound<DefaultResponse>>(res);

        var okResult = (NotFound<DefaultResponse>)res;
        Assert.Equal(StatusCodes.Status404NotFound, okResult.StatusCode);
    }
}