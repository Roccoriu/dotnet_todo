namespace TodoApp.Models;

public class User {
    public int Id { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
    public string PasswordHash { get; set; } // Store hashed passwords, not plain text
    public ICollection<TodoItem> TodoItems { get; set; }
}