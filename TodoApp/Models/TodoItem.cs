using FluentValidation;

namespace TodoApp.Models;

public class TodoItem {
    public int Id { get; set; }
    public string Title { get; set; }
    public string? Description { get; set; }
    public bool IsComplete { get; set; }
    public DateTime? DueDate { get; set; }
    public int UserId { get; set; } // Foreign key
    public User User { get; set; } // Navigation property
    public int? CategoryId { get; set; } // Nullable foreign key
    public Category Category { get; set; } // Navigation property
}

public record GetTodoItemDto(
    int? Id,
    string Title,
    string? Description,
    bool IsComplete,
    string CategoryName,
    DateTime? DueDate
);

public record CreateTodoItemDto(
    string Title,
    string? Description,
    bool IsComplete,
    DateTime? DueDate
);

public class TodoItemValidator : AbstractValidator<CreateTodoItemDto> {
    public TodoItemValidator() {
        RuleFor(m => m.Title).NotEmpty();
        RuleFor(m => m.Description).MaximumLength(5000);
        RuleFor(m => m.IsComplete).NotNull();
    }
}